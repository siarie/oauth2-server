package main

import (
	log "log/slog"
	"net/http"
	"os"

	"github.com/gorilla/sessions"
	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"siarie.me/p/oauth2-server/pkg/renderer"
)

type JSONResponse struct {
	Error bool `json:"error"`
	Message string `json:"message,omitempty"`
	Data interface{} `json:"data,omitempty"`
}

func main() {
	e := echo.New()
	e.Static("/static", "static")
	e.Use(middleware.Logger())
	e.Use(session.Middleware(sessions.NewCookieStore([]byte("secret"))))
	r, err := renderer.New("templates")
	if err != nil {
		panic(err)
	}
	e.Renderer = r

	e.Use(middleware.CSRFWithConfig(middleware.CSRFConfig{
		TokenLookup:    "form:_csrf,header:X-CSRF-Token",
		CookiePath:     "/",
		CookieHTTPOnly: true,
		CookieSameSite: http.SameSiteStrictMode,
		// TODO
		// CookieDomain:   "example.com",
		// CookieSecure:   true,
	}))
	e.GET("/login", func(c echo.Context) error {
		data := map[string]interface{}{
			"csrf": c.Get("csrf"),
		}
		return c.Render(http.StatusOK, "auth/login.html", data)
	})

	e.POST("/login", func(c echo.Context) error {
		username := c.FormValue("username")
		password := c.FormValue("password")

		if username != "siarie" && password != "123456" {
			return c.String(http.StatusOK, "invalid credentials")
		}

		sess, err := session.Get("_oauth2", c)
		if err != nil {
			log.Error("http/server: session error", "error", err)
			return c.String(http.StatusInternalServerError, "internal server error")
		}

		sess.Options = &sessions.Options{
			Domain:   "localhost",
			Path:     "/",
			MaxAge:   86400 * 7,
			HttpOnly: true,
			SameSite: http.SameSiteStrictMode,
			// Secure: true, TODO
		}

		sess.Values["username"] = username
		sess.Values["logged_in"] = true
		sess.Save(c.Request(), c.Response())

		return c.Redirect(http.StatusSeeOther, "/")
	})

	// api route
	// this is route that will be protected with oauth2
	api := e.Group("/api")
	api.GET("", func (c echo.Context) error {
		res := JSONResponse{
			Error: false,
			Message: "welcome to the black parade.",
		}
		return c.JSON(http.StatusOK, &res)
	})

	// private route
	pr := e.Group("")
	pr.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			sess, err := session.Get("_oauth2", c)
			if err != nil {
				log.Error("http/server: session error", "error", err)
				return err
			}

			loggedIn, ok := sess.Values["logged_in"]
			if ok && loggedIn.(bool) {
				next(c)
			}

			return c.Redirect(http.StatusSeeOther, "/login")
		}
	})
	pr.GET("/", func(c echo.Context) error {
		return c.Render(http.StatusOK, "index.html", map[string]interface{}{
			"user": "siarie",
		})
	})

	srv := http.Server{
		Addr:    "127.0.0.1:9999",
		Handler: e,
	}

	log.Info("http: server started.", "listen", srv.Addr, "pid", os.Getpid())
	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		log.Error("http: server stopped", "error", err)
	}
}
