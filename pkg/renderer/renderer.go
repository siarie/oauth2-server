package renderer

import (
	"html/template"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/labstack/echo/v4"
)

type Renderer struct {
	templates *template.Template
}

func parseTemplates(root string, funcs template.FuncMap) (*template.Template, error) {
	tpl := template.New("")
	pfx := len(root) + 1
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if !info.IsDir() && strings.Contains(path, ".html") {
			ts, err := ioutil.ReadFile(path)
			if err != nil {
				return err
			}

			_, err = tpl.New(path[pfx:]).Funcs(funcs).Parse(string(ts))
			if err != nil {
				return err
			}
		}

		return err
	})

	if err != nil {
		return nil, err
	}

	return tpl, nil
}

func New(templateDir string) (*Renderer, error) {
	tplFuncs := template.FuncMap{
		"hello": func(obj string) string {
			return "Hello, " + obj
		},
	}
	tpl, err := parseTemplates(templateDir, tplFuncs)
	if err != nil {
		return nil, err
	}

	// fmt.Println(tpl)
	return &Renderer{
		templates: tpl,
	}, nil
}

func (r *Renderer) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return r.templates.ExecuteTemplate(w, name, data)
}
